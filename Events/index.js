/* Event Bubbling vs Event Capturing */

                /*

            |   window     A
            |   document   |
            |   body       |
     event  |   first      |  event 
 capturing  |   second     |  bubbling
     phase  |   third      |  phase
            |   fourth     |
            V   button     |

                */


<body class = "body item">
    <div class = "first item">
        <div class = "second item">
            <div class = "third item">
                <button class = "fourth item"></button>
            </div>
        </div>
    </div>
</body>

//this will cause the event to fire from the inside out
// (event bubbling
//change to true for event capturing)
let item = document.getElementsByClassName("item");
for(let i = 0; i < items.lenght; i ++){
    (function () {
        let y = i;
        items[y].addEventListener("click", function (event) {
            console.log(item[y], event);
        }, false);
    })();
}


/* stopPropagation and preventDefault */

// stop event from going down the event capturing phase
//or up the event bubbling phase
let item = document.getElementsByClassName("item");
for(let i = 0; i < items.lenght; i ++){
    (function () {
        let y = i;
        items[y].addEventListener("click", function (event) {
            if(y == 2){
                event.preventPropagation();
            }
        }, false);
    })();
}   

//preventDefault stops the given behvour that would take place
let item = document.getElementsByClassName("item");
for(let i = 0; i < items.lenght; i ++){
    (function () {
        let y = i;
        items[y].addEventListener("click", function (event) {
            if(y == 2){
                event.preventDefault();
            }
        }, false);
    })();
}   