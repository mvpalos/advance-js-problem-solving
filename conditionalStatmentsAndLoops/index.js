// 1. javascript program that accepts two int and displays larger

const largerInt = (first, second) => {
    if(first > second){
        return first;
    }
    else return second;
}

largerInt(1, 3)

// 2. Write a for loop that will iterate from 0 to 20. For each iteration, 
//    it will check if the current number is even or odd, and report that to 
//    the screen (e.g. "2 is even").

const oddOrEven = () => {
    let numbers = [1,2,3,4,5,6,7,8,9,10]
    numbers.forEach((number) => {
        if(number % 2){
            return console.log(number, "is odd")
        }
        else return console.log(number," is even")
    })
}

// 3. Write a for loop that will iterate from 0 to 10. For each iteration of 
//    the for loop, it will multiply the number by 9 and log the result 
//    (e.g. "2 * 9 = 18")

const multiplyBy9 = () => {
    let numbers = [1,2,3,4,5,6,7,8,9,10];
    numbers.forEach((number) => {
        return number * 9;
    })  
}

