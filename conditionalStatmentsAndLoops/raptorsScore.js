// Getting stats from the Raptors last game in the 2018 season
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// Requirements
// 1. There are 4 functions to fill in.
// 2. Some funtions will use other funtions to complete their answers
// 3. Avoid using for, forEach, for...in or for...of

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// 1. Count all the points a team had in the game.
//   - Write a function to count the total points Raptors had per quarter
//     - 1st quarter - 26
//     - 2nd quarter - 21
//     - 3rd quarter - 25
//     - 4th quarter - 21

const totalScore = (pointsPerQuarter) => {
    pointsPerQuarter.reduce((acc, point) => {
        return acc + point;
    }, 0);
};

const raptorsScore = totalScore([26, 21, 25, 21]);

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// 2. Get the average points per starting player
//   - Write a function to get get the average points scored by the starting raptors
const startingPlayers = {
  seong: {
    points: 12,
  },
  jeff: {
    points: 13,
  },
  jordan: {
    points: 2,
  },
  raj: {
    points: 5,
  },
  shufei: {
    points: 13
  },
};

const getAveragePoints = (players) => {
    let totalScore = Object.keys(players[player].reduce((acc, player) => {
        return players[player].points + acc;
    }, 0))
    let totalPlayers = Object.keys(players).length;

    return totalScore / totalPlayers;
};

const averageScore = getAveragePoints(startingPlayers);


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// 3. Remove the starting players who scored less than the average
//   - Write a function to remove the starting players who scored less than the average
//   - The return value of the function should be in the same format as the `startingPlayers`
//     object above
//   - Make use of `startingPlayers` and `averageScore` that were created in question 2

const getHighestScorers = (players, threshold) => {
    return Object.keys(players).filter((player) => players[player].points > threshold)
};

const highestScoringPlayers = getHighestScorers(startingPlayers, averageScore);