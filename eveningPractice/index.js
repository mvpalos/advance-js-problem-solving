// 1. Given an array of integers, return indices odd the two numbers such
//    that they add up to a specific target.

//    you may assue that each input has exactly one solution
//    and you may not use the same input twice.

// example: Given nums = [2, 7, 11, 15], target = 9
//          Because nums [0] + nums[1] = 2 + 7 = 9
//          return [0, 1].

const twoSum = (nums, target) => {
    for(let i = 0; i < nums.length; i++ ){
        for(let j = i + 1; j < nums.length; j++){
            if(nums[j] === target - nums[i]){
                return i, j;
            }
            else console.log("no solution! :(")
        }
    }
}

// 2. Write a function to find the longest common prefix string
//    amongst an array of strings. If there is no common prefex
//    return an empty string

// example: input ["flower", "flow", "flight"]
//          output: "fl"

const longestCommonPrefix = (strs) => {
    if(strs.length == 0) return "";
    let prefix = strs[0];
    for(let i = 1; i < strs.length; i++){
        while (strs[i].indexOf(prefix) != 0){
            prefix = prefix.substring(0, prefix.length() -  1);
            if(prefix.isEmpty()) return "";
        }
        return prefix;
    }
}


// 3. Add two numbers
//    You are given a non-empty linked lists representing two non-negative integers.
//    The digits aree storeed in reverse order and each of the nodes contains a single digit.
//    Add the two numbers and return the linked-list.

// example: Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
//          Output: 7 -> 0 -> 8
//          Explination: 342 + 465 = 807



// 4. Maximum Subarray
//    Givenan integer array nums, find the contiguous subarray (containing at least one number) which  has
//    the largest sum and return the sum 

// example: Input: [-2,1,-3,4,-1,2,1,-5,4]
//          Output: 6
//          Explination: [4,-1,2,1] has the largest sum = 6.
