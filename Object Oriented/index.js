/* THIS is determined upon calling context <== IMPORTANT */ 
//usre strict stops THIS from being global object which
//causes most of the problems

// var jordan = {
//     checkThis : function(){
//         var self = this;
//         //this keyword is pointing to object jordan
//         console.log(self)
//         // logs object{}

//         function checkOther(){
//             console.log(self)
//             //points to self which is an object inside of jordan 
//             self.moo = 1;
//         }
//         checkOther();

//         console.log(self.moo);
//         console.log(window.moo);
//     }
// };

/* CALL, BIND, APPLY */

/* CALL */
//the first parater being called is variable and the second is paramas
//the function being called

var jordan = {
    checkThis: function(){
        function checkOther(){
            //this
            console.log(this);
        }
        checkOther.call(this); 
        //by using call we are calling the object jordan
        //only applies when we are using strict
        //call is used to stabalize the call of what 'this' is
        //functions are obejct with other properties ei=> .call
    }
}

jordan.checkThis()

/* APPLY */
//used when passing in an array as an argument
//you can pass asa a variable array of numbers 
//which gets passed in as an arguments array
//(all functions have an argument array)

function sum(){
    var total = 0;
    for(var i = 0; i < arguments.length; i++){
        total += arguments[i];
    }
    return total;
}

var things = [1,2,3,4,5,4,32,]
var x = sum.apply(null, things);
console.log(x)

/* BIND */
//bind can only be used on functions after they have
///been created and applied to a variable

//(still using strict)

var sayHello = function(last_name) {
    console.log("hello" + this + last_name)
}.bind("jordan"); //fisrt param

sayHello("villanueva") //second param

/* PROTOTYPE CHAIN */

var device = {
    kind: "music player"
};

var product  = Object.create(device)
console.log(product.kind);

/* CLASSICAL VS PROTOTYPICAL INHARITANCE */

//classical => methods of OO in older languges like java and c++
            //clase acts a a blueprint or design
            //and create instance
//Protoypal ==> new objects are created from exiting objects
                //we build a house based on another house

/* Constructor OO Pattern */

function Person(first_name, last_name) {
    this.first_name = first_name;
    this.last_name = last_name;
    this.full_name = function(){
        return this.first_name + ' ' + this.last_name; 
    }
}

var dude = new Person("asim", "hussain");
console.log(dude.full_name);

//"new" creates an instance of the person class
//constains a property

//use of closure in prototypical inheritance
function Person(first_name, last_name){
    this.first_name = first_name;
    this.last_name = last_name;
    this.full_name = function() {
            return first_name + ' ' + last_name;
            //this will retun the refrence of person in the
            //of the arguments passed in the previouse function
            //without destroying them
        };
};

Person.prototype.full_name = function(){
    return this.first_name + ' ' + this.last_name;
}
//this will link the full_name function into the prototype

var dude = new Person('asim', 'hussin');
//this will now create an instance of the object of dude
//from the person constructor

console.log(dude.full_name());

/* Constructor OO Pattern PART 2 (INHERITANCE) */

function Person(first_name, last_name){
    this.first_name = first_name;
    this.last_name = last_name;
}

Person.prototype.full_name = function(){
    return this.first_name +  ' ' + this.last_name;
};

function Professional(honourific, last_name, fisrt_name){
    Person.call(this, first_name, last_name);
    this.honourific;
}

Professional.prototype = Object.create(Person.prototype);

Person.prototype.full_name = function() {
    return  this.first_name = first_name + ' ' + last_name;
}

var prof = new Professional("Dr", "Jordan", "Villanueva");
console.log(prof.Professional_name());
console.log(prof.full_name());