'use strict'
//strict makes debugging easier by 
// 1. reserving key words.
// 2. checking misspelings. etc
// 3. cannot use variables that have not been delcared with var first (linked to 2)

// let a = 1;
// // primative types pass values ==> passing a copy
// // objects pass refrences
// function foo(a){
//     a = 2;
//     // anything done to this 'a' will not effect outer 'a'
//     // this is because this is a copy of 'a' and not actual 'a'
// }

// foo(a);
// console.log(a) // console logs ==> 1

/*BUT WHEN B IS REFRENCED AS OBJECT */

// let b = {moo:true};
// function foo(b){
//     b.moo = false
// }

// foo(b)
// console.log(b)
//console log is no longer empty but rather becomes filled with 'false'

/*IF YOU ARE ATTEMPTINGTO CHANGE WHAT AN OBJECT POINTS TO */

// you cannot change what object points to. Only the property
//ei.

// let a = {"moo": "too"}

// function foo(a){
//     a.moo = {"too" : "moo"}
// }

// foo(a)
// console.log(a)


/* DYNAMICALLY TYPED LANGUAGES */

// types of variable is determined at runtime
// vs in java when you are typing statically throws issue right away 
// let a = 'moo';
// typeof(a);
// a = 1;

// console.log(a) // will return 1

/* == vs === */

// (equality) == equality of values on the left and right
// (strict equality) === checks for type and value equality

/* DIFFRENT Scoping IN VARIABLE */

//functions and variables arehoisted to the top of the page
//but a variable equal to an anynymoue function will 
//the variable will the hoisted to the top of the page
//while the function remains at the bottom Ei...

// let foo;
// foo();

// foo = function(){
//     let a;
//     console.log(a);
//     a = 1;
// }

/* FUNCTION CHAIN */
//when one function needs to use a perticualr function it looks up the function chain
//scope chain is defined lexico --> in the orfer that it is defined in the file

// function goo(){
//     let myvar = 1;
//     function foo(){
//         console.log(myvar)
//     }
//     foo()
// }
// goo()

/* IIFE (Immideatly Invoked Function Expression) */

//THIS CREATES GLOBAL VARIABLE
// let thing = {"hello" : "main"};
// console.log("main", thing)

//REMOVING GLOBAL VARIABLE
//first place the global variable in function to scope the variable locally
//then create the function to be called locally

// (function(){
//     let thing = {"hello" : "main"}
//     console.log("main", thing);
// })();

// this will now cause an ERROR when calling thing globally

/* FUNCTION CLOSURE */

// function sayHello (name) {
//     let text = "Hello " + name
//     console.log(text)
// }

// let sayJordan = sayHello("jordan");

// console.log(sayJordan)

//usually with var when making a refrence outside of the scope of the function, variable will be deleted
//when a function returns a function it makes refrence to a variable on local > closure > global to find
//what it is looking for.

function sayHello(name) {
    var text = "Hello " + name;
    return function(){
        console.log(text)
    }
}

let sayJordan = sayHello("Jordan")();

/* Closure Interview Test Question */
//This will print out 10/10/10 on console

//becuase using var (not let) array will exaust itself and only call foo once the whole
//loop is done and will call [ 10, 10, 10 ]

// var foo = [];
// for(var i = 0; i < 10; i++){
//     foo[i] = function() { return i }
// }

// console.log(foo[0]());
// console.log(foo[1]());
// console.log(foo[2]());

// this willl create array call of [1,2,3]
// when returning a function the whole scope of the function is being called

var foo = [];
for(var i = 0; i < 10; i++){
    (function(){
        var y = i;
        foo[i] = function() {
            return y //stored refrence to y variable
        }
    })(); //when this immidiatly invoced function exited y variable is deleted
            // but because inner closure needed it, it stored a refrence to y variable
}

console.log(foo[0]());
console.log(foo[1]());
console.log(foo[2]());