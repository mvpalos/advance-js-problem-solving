"use strict"

// let thing = {"hello": "main"};
// console.log("other", thing)

//make a function invokable by wrapping it in a function and it will be called in the order that it is
//brought in by the html and css file\

//THRERE IS NO MORE MAIN VARIABLE if ther are all wrapped in a functione xpression

(function(){
    let thing = {"hello": "main"}
    console.log("other", thing)
})();