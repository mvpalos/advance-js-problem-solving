/* Stack and Queue */

// javascript engiens have a call stack and message queue
// that executes your code at runtime.
// When you hit 'undo' in your text editor or 'back' in your
// browser you are using a stack.

// STACK
const stack = [1,2,3,4];

stack.push(5);
stack.pop();

// -> 5
// [1,2,3,4]

// QEUEU
const queue = [1,2,3,4,5];

queue.enqueue(5);
queue.dequeue()

// -> 1
// [2,3,4,5]

/* LinkedList */

// LL are often the underlying data structure for a stack or queue.
// you can impliment a Least Recently Used cache using a LL
// hash tables also use linkedlist to handle collisions

const linkedlist = {
    head: {
        value : 1,
        next: {
            value: 2,
            next: {
                value: 3,
                next: null
            }
        }
    }
}

// DOOUBLE LINKED LIST

// always set as undefined to show that you had purpouslt implimented as so,
// undefined is set at runtime

const linkedlist = {
    head: {
        value : 1,
        next: {
            value: 2,
            next: {
                value: 3,
                next: null,
                previouse: {Pointer} // point to node so that 
                                     //is value is changed wont be effected
            }
        }
    }
}



/* Hash Table */

// implimentated when solving a problem that requires a fast look up
// passing a sting and return a number

// used in Object/Map/Set

String.prototype.hashCode = () => {
    let hash = 0, i, chr;
    if(this.length === 0) return hash;
    for(i = 0; i < this.length; i++){
        chr = this.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0;
    }
    return hash;
}

/* Array / String */






/* STACK IMPLIMENTATION */

// notes: ._ used if do not want to access method directly

class Stack {
    constructor() {
        this._storage = {};
        this._length = 0;
    }

    push(value) {
        this._storage[this._length] =  value;
        this._length++;
    }

    pop() {
        const lastVal =  this._storage[this._length - 1];
        // also delete with delete key word
        this._storage[this._length - 1] = undefined;
        this._length--;
        return lastVal;
    }

    // looking at final index without modifying 
    peek() {
        if(this._length) {
            return this._storage[this._length - 1];
        }
    }
}

const myStack = new Stack();


/* Queue IMPLIMENTATION */

class Queue {
    constructor(){
        this._storage = {};
        this._length = 0;
        this._headIndex = 0;
    }

    enqueue(value) {
        const nextIndex = this._length + this._headIndex;
        this._storage[nextIndex] = value;
        this._length++;
    }

    dequeue() {
       let firstValue = this._storage[this._headIndex];
       this._storgae[this._headIndex] = undefined;
       this._length--;
       this._headIndex++;
       return firstValue;
    }

    peek() {

    }
}

let myQ = new Queue();
myQ.enqueue('zero');



/* Linked List IMPLIMENTATION */

// refrence head to tail it vital
// usually used when attempting to operate from begining or
// the end of a data strucutre

class LinkedList {
    constructor(value) {
        this.head = { value, next: null };
        this.tail = this.head;
    }
    insert(value) {
        // update tail
       const node = { value, next: null };
       this.tail.next = node;
       this.tail = node;
    }

    remove(value) {
        let currentNode = this.head;
            while(currentNode.next != this.tail){
                currentNode = currentNode.next;
            }
            currentNode.next = null;
            this.tail = currentNode;
    }

    isHead(node) {
        return node === this.head;
    }

    isTail() {
        return node === this.tial;
    }

    contians(value) {
        let currentNode = this.head;
        while(currentNode !== value) {
            currentNode = currentNode.next;
        }
        return currentNode.value === value;
    }
}

let myList = new LinkedList();
myList.inset(2);
myList.remove(2);
myList.isHead();
myList.isTail();
myList.contians();



/* Hash Tabl IMPLIMENTATION */

// insert and remove in constant time

class HashTable {
    constructor(val) {
        this._storage = [];
        this._tableSize = val;
    }

    insert(key, value) {
        // getting index
        const index = this._hash(key, this._tableSize);
        // this function alonw will not avoid collisions
        // this will place value into storage

        // IF NO COLLISION AVOIDANCE, INDEX WILL BE OVERWRITTEN
        this._storage[index] = value;
    }

    insertToAvoidCollisions(key, value) {
        const index = this._hash(key, this._tableSize);

        if (!this._storage[index]){
                this._storage[index] = [];
        }
        // [0, 0, 0, []]
        // TODO: Loop through array to see if key value already inserted
        this._storage[index].push([key, value]);
        // [0, 0, 0, [['a', 1], ['b', 2]]]
    }

    remove(key) {
        const index = this._hash(key, this._tableSize);
        const arrayAtIndex = this._storage[index];

        if(arrayAtIndex) {
            for(let i = 0; i < arrayAtIndex.length; i++){
                const firstValue = arrayAtIndex[i];
                    if(firstValue[0] === key) {
                        delete firstValue[1];
                    } 
            }
        }
    }

    retrieve(key) {
        const index = this._hash(key, this._tableSize);
        const arrayAtIndex = this._storage[index];
        //checking for collisions
            if(arrayAtIndex) {
                for(let i = 0; i < arrayAtIndex.length; i++) {
                    const keyValueAarray = arrayAtIndex[i];
                        if(keyValueArray[0] === key) {
                            return keyValueAarray[1];
                        }
                }
            }
    }

    // hello / 5
    hash(str, n) {
        let sum = 0;
            // going though sting
            for(let i = 0; i < str.length; i++) {
                // charCodeAt() method returns an integer between 
                // 0 and 65535 representing the UTF-16 code unit at the given index.
                // this is to assign according to sting length of index
                sum += str.charCodeAt(i) * 3;
                // this is to divid for total length of array
                return sum % n;
            }
    }   
}

