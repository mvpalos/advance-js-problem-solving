/* Intro to Space and Time Complexity */

// Space Complexity -> how much memory is needed for a function
// Time Complexity -> how many operations are needed for a function

// n -> looping once and comparing
// n^2 -> quadratic comparison (as we loop through once we double # by power)
//      (compare all numbers)
// 2n -> going through 2 diffrent loops
//       (F find min and max)
// 2 -> sorted list, find first and last

/* Big-O, name */

// O(n^2), quadratic -> n^2
// O(n), linear -> 2n
// O(1), constant -> 2

/* TC example */

// constant time algorithm
let countChars = (str) => {
    return str.length
}
countChars("dance");
countChars("walkreallyfast")

// not constant operation
let myList = ["hello", "hola"];
// constant
myList.push("bonjour");
//not constant because first method must be removed and shifts index down
myList.unshift();
myList.shift();

// under the hood contant time vs not constant

let countChats = (str) => {
    let count = 0;
    for(let i = 0; i < str.length; i++) {
        count++;
    }
    return count;
}

/* OPTOMIZING YOUR CACHING */

const isUnique = (arr) => {
    let result = true;

    for(let i = 0; i < arr.length; i++) {
        console.log(`--- OUTER LOOP --- i === ${i}`);
            for(let j = 0; j < arr.length; j++) {
                console.log(`--- INNER LOOP --- j === ${j}`);
                 if(i !== j && arr[i] === arr[j]) {
                     result = false;
                 }
            }
    }
    return result;
}

console.log(isUnique([1,2,3]) ===  true)
console.log(isUnique([1,2,3]) === true)

// more efficient version with single loop

const isUniqueSingleLoop = () => {
    const breadCrumbs = {};
    let result = true;

    for(let i = 0; i < arr.length; i++) {
        console.log(`--- LOOP --- ${i}`);
            if (breadCrumbs[arr[i]]) {
                result = false;
            }
            else {
                breadCrumbs[arr[i]] = true;
            }
    }
    return result;
}

console.log(isUniqueSingleLoop([1,2,3]) ===  true)
console.log(isUniqueSingleLoop([1,2,3]) === true)

// Unique Sort Exerecise

// Task: transform this simple sorting algorithm into a unique sort.
// It should not return an duplicate values in the sorted array.

// input: [1,5,2,1] => output: [1,2,5];
// input: [4,2,2,2,3,2,2] => output: [2,3,4]

const uniqueSortEx = () => {
    const breadCrumbs= {};
    const result = [arr[0]];

    for(let i = 1; i < arr.length; i++) {
        // start loop at 1 as element 0 can never be duplicated
        if(!breadCrumbs[arr[i]]) {
            result.push(arr[i]);
            breadCrumbs[arr[i]]
            breadCrumbs[arr[i]] = true;
        }
    }
    return result.sort((a, b) => a - b);
}

uniqueSort([4,2,2,2,3,2,2]);

/* MEMOIZATION */

// Caching the result of a function

const factorial = (n) => {
    // Calculations: n * (n-1) * (n-2) * ... (2) * (1)
    return factorial
}

factorial(35)

// Memoization Exerecise

// Task 1: write a function, times10, that takes an argument, n and multiplies n times 10
// a simple multiplication fn

const times10 = (n) => {
    return n * 10; 
}

console.log('-----TASK 1-----');
console.log('times10 return:', times10(9));

// Task 2: Use an object to cache the results of your times10 function.
// proTip 1: Create a function that checks if the value for n has been calculated before
// proTip 2: If the value m has not been calculated, calculate and then save the result in the cache object

const cache = {};

const memoTimes = (n) => {
    if (n in cache) {
        console.log('fetching from n', n);
        return cache[n];
    }
    else {
        console.log('calculating result')
        let result = times10(n);
        cache[n] = result;
        return result;
    }  
}

console.log('----TASK 2----');
console.log('Task 2 calculated value:', memoTimes10(9)); // calculated
console.log('Task 2 cached value:', memoTimes(9)); // cached

// Memorization with closure Exerecise

// Task 3: Clean up your global scope by moving your cache inside your function
// protip : use closure to return a function that you can call later

const memoizedClosureTimes10 = () => {
    const cache = {};
        return (n) => {
            if (n in cache) {
                console.log('Fetching from cache:', n);
                return cache[n];
            }
            else {
                console.log('calculating results');
                let result = times10(n)
                cache[n] = result;
                return result;
            }
        }
}

const memoClosureTimes10 = memoizedClosureTimes10();
console.log('---- TASK 3---');
    try {
        console.log('Task 2 calculated value:', memoClosureTimes10(10));
        console.log('Task 3 cached value:', memoClosureTimes(9));
    }
    catch{
        console.log('Task 3:', e)
    }

// Generic Memoize Function Exercise
// note: memoization is used not for constant operations, giving a time complex improvment of none
// but when doing a callback with very time expensive operations, the benefit will kick in

const memoize = () => {
    let cache = {};
    return (n) => {
        if(n in cache) {
            console.log('Fetching from cache', n);
            return cache[n];
        }
        else {
            console.log('Calculating result');
            let result = cb(...arg);
            cache[n] = result;
            return result;
        }
    };
};

const memoizedTimes10 = memoized(times10);
console.log('Task 4 calculate value:', memoizedTimes10(9));

/* PERSONAL NOTES */

// for each operation you are trading time complexity for space complexity
// for each operation you are creating new data structure and adding data to it each time.
// Therefore increasing the space complexity while minimizing the time complexity
// important when your function call is 'expensive'.

// use a hash table to optomize??? ---> object is esentailly a hash table
// we are adding thing to a chache which is really just an object

// HOW DO WE TAKE AN EXPEENSIVE OPERATION AND TURN IT INTO CONTANT TIME?!??!?!?!!??!?
//(USIN A HASH TABLE...)




/* INTRO TO RECURSION */

// at its core, when a function calls itself

// 1. Push called Fn on stack
// 2. Execute Fn body. until ...

// ... another rn is called:

//     Pause the current execution and start at step 1.
// ... return it hit:

//     Pop the current Fn off the stack
//     Resume executing the previous Fn


let tracker = 0;

let callMe = (arg) => {
    tracker++
    if(tracker === 3) {
        return `loops ${args}`
    }
    return callme('anytime')
}

callMe();

const loopTime = (n) => {
    console.log("n=", n)
    if(n <= 1 ){
        return 'complete'
    }
    return loopTime(n-1);
}

// Factorial Recursion

// creating a loop that will pring the result of the previouse multiplication

let computeFactorial = (num) => { // 2
    let result = 1;

    for(let i = 2; i <= num; i++){
        console.log(`result = ${result} * ${i} (${result})`)
        result *= i
    }
    return result;
}

// recersive example

let computeFactorial = (num) => {
    if(num === 1) {
        console.log('hitting base case')
        return 1;
    }
    else {
        console.log(`returning ${num} * computeFactorial(${num -1})`)
        return num * computeFactorial(n-1);
    }
}

computeFactorial(5)

// RECURSION VS LOOPING

/* IMPORTANT SELF NOTES (things to think about) */

// 1. What is your base case?
// 2. How do you get close?
// 3. What is the work needed within your function body?

// RECURSION --> can be easier to impliment than a loop
//              some data structires have deeply nested things


// WRAPPER FUNCTIONS

// first create a function that retains access to the function with recursion

let wrapperFnLoop = (start, end) => {
    let recurse = (i) => {
        console.log(`loop start ${start}, until ${end}`)
            if(i < end) {
                recurse(i + 1);
            }
    }
    recurse(start)
}

// this will not retain access to the closure 

let MemoFnLoop = (i, end) => {
    console.log(`looping from ${i} until ${end}`)
    if(i < end) {
        memoFnLoop(i + 1, end)
    }
}


// ACCUMULATOR

let joinElements = (array, joinString) => {
    let recurse = (index, resultSoFar) => {
        resultSoFar += array[index];

        if(index === array.length -1) {
            return resultSoFar;
        }
        else {
            return recurse(index + 1, resultSoFar + joinString)
        }
    }
    return recurse(0, '');
}

joinElements(['s','cr','t cod', ':)', ':)', 'e']);

// TASK: rewrite function above si that it uses a loop rather than recursion

let joinElements = (array, joinSting) => {
    let resultSoFar = '';
    for(let i = 0; i < array.length - 1; i++) {
        resultSoFar += array[i] + joinString;
    }
    return resultSoFar + array[array.length - 1];
}

joinElements(['s','cr','t cod', ':)', ':)', 'e']);

/* RECURSIVE FACTORIAL & MEMOIZE */

const memoize = (fn) => {
    let cache = {};
    return (...args) => {
        let n = args[0];
            if(n in chache) {
                console.log('fetching from cache', n);
            }
            else {
                console.log('Calculation Result');
                let result = fn(n);
                cache[n] = result;
                return result;
            }
    }
}



const factorial = memoize(
    (x) => {
        if(x === 0) {
            return 1;
        }
        else {
            return x * factorial(x - 1)
        }
    }
);

/* DIVIDE AND CONQUER */

// ei. binary search --> dividing the array in half and decreasing each time
//                       THE ARRAY MUST BE SORTED


// Linear Search
// constant time operation loops through every index in the array
// worst case at the end of the array, best case at start

let lineachSearch = (list, item) => {
    let index = -1;
    list.forEach((listItem, i) => {
        if(listItem === item) {
            index = i;
        }
    })
}

lineachSearch([2,6,7,90,103], 90);


// Binary Search
// break out list in half (ARRAY MUST BE SORTED)
// is where we break the array less than or greater than what we are searchig for

// arguments : [2,6,7,90,103], 90
let binarySearch = (list, item) => {
    let min = 0;
    let max = list.length - 1;
    let guess;

    while (min <= max) {
        guess = Math.floor((min + max)/2);
        if(list[guess] == item) {
            return guess;
        }
        else {
            if (list[guess] < item) {
                min = guess + 1;
            }
            else {
                max = guess -1;
            }
        }
    }
    return -1;
}

// self notes :
// recognize base case
// 1. Divide: breack down problem down during each call
// 2. Conquer: do work for each subset
// 3. Combine: solition

// NAIVE SORTS : keep looping and comparing values till end of list
//               ei. Bubble Sort, Insertion Sort, Selection Sort

// MERGE & CONQUER SORT : recursivly divide lists and sort smaller parst of list until entire list is sorted
//                         ei. Mergesort, Quicksort


// Bubble Sort

let bubbleSort = (array) => {
    let counterOuter = 0;
    let counterInner = 0;
    let counterSwap = 0;

    for (let i = 0; i < array.length; i++) {
        counterOuter++;
        if(array[j - 1] > array[j]) {
            counterSwap++;
            swap(array, j - 1);
        }
    }
}

// optomized swap

let optomizedBubbleSort = () => {
    let counterOuter = 0;
    let counterInner = 0;
    let counterSwap = 0;

    let swapped;

        counterOuter++;
        swapped = false;
             for(let i = 0; i < array.length; i++) {
                counterInner++;
                    if(array[i] && array[i] && array[i] > array[i +1]) {

                    }
            }
            while(swapped);
            return array;
}

// IMPLIMENTATION OF MERG SORT

// split this array into halves and then merge them recrusivly
let mergSort = (arr) => {
    if (arr.length === 1) {
        // return once we hit an array with a single item'
        return arr
    }

    const middle = Math.floor(arr.length / 2); //get the middle eof the array and round down
    const left = arr.slice(0, middle); // itmes on the left side
    const right = arr.slice(middle); // items on right side
    const sortedLeft = mergeSort(left);
    const sortedRight = mergeSort(right);
    return merge(sortedLeft, sortedRight)
}

//compare the arrays items and return the concacted results

let merge = (left, right) => {
    let result = [];
    let indexLeft = 0;
    let indexRight = 0;

    while(indexLeft < left.length && indexRight < right.length){
        if (left[indexLeft] < right[indexRight]) {
            result.push(left[indexLeft])
            indexLeft++
        }
        else {
            result.push(right[indexRight])
            indexRight++
        }
    }
    return result.concat(left.slice(indexLeft)).concat(right.slice(indexRight))
}

//GREEDY ALGORITHMS

// always make the locally optimal choice

// ei. coin value 5, 10, 25
// use least number of coins that add up to an amount where the amount is always
// divisable by 5

// input amount: 40, output # of coins: 3 (25, 10, 5)
// input amount: 35, output # of coins: 2 (25, 10)


const makeChange = (coins, amount) => {
    coin.sort((a,b) => b - a);
    let coinTotal = 0;
    let i = 0;
    while (amount > 0 ) {
        if (coins[i] <= amount) {
            amount -= coins[i];
            coinTotal++
        }
        else {
            i++
        }
    }
    return coinTotal;
};

makeChange([5, 10, 25], 5);

// input amount 40, output # of coins: 3 (25, 10, 5)

makeChanges([5, 10, 25], 50);

// Brute Force

// would these values work with your greedy solution?
// coin values: 1,6,10
// input: 12


// this case would require the brute force approach, where you calculate every singel
// combination possible and keep track of the minimum (recursion) 

const makeChange = (value, i) => {
    recursionCounter++;
    console.log(`${recursionCounter}: calling ${value} at ${i}`)
    if(value === 0) return 0;
    let minCoins;
    coins.forEach((coin, i) => {
        if(value - coin >= 0) {
            let currMinCoins = makeChange(value - coin, i);
                if(minCoin === undefined || currMinCoins < minCoins) {
                    minCoins = currMinCoins;
                }
        }
    });
    return minCoins + 1;
}

// INTRO TO DYNAMIC PROGRAMMING 

// an optomization technique
// top down approachwhere you cache / buttom up

// DP Qualities:
// Optimal Substructure (tends to be recursuve)
// Overlapping Subproblems

// DP vs Divide and Conquer

// DP Approaches:

// Top Down (recursive) vs Bottom Up (Iterative)
/* Self Notes: LOOL MORE INTO DYNAMIC PROGRAMMING !!!!  ༼ つ ಥ_ಥ ༽つ  */ 



/* MEMOIZATION AND RECURSION */

const cache = {};
const coins = [10, 6, 1];

const makeChange = (c) => {
    // Return the value if it's in the cache
    if (cache[c]) return cache[c];

    let minCoins = -1;

    // Find the best coin
    coins.forEach(coin => {
        if(c - coin >= 0){
            let currMinCoins = makeChange(c - coin);
            if(minCoins === -1 || currMinCoins < minCoins) {
                minCoins = currMinCoins
            }
        }
    })
}

